<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\UserPayment;

class UserController extends Controller
{
    /**
     * Function to return register view
     */
    public function register() {
        return View::make('register');
    }
    
    /**
     * Function to register user.
     * @param Request $request
     * @return type JSON
     */
    public function registerUser(Request $request) {
        try {
            \DB::beginTransaction();
            parse_str($request['data'], $formData);
            $userDetails = new User;
            $userDetails['first_name'] = $formData['first_name'];
            $userDetails['last_name'] = $formData['last_name'];
            $userDetails['telephone'] = $formData['telephone'];
            $userDetails['street'] = $formData['street'];
            $userDetails['house_no'] = $formData['house_no'];
            $userDetails['zip_code'] = $formData['zip_code'];
            $userDetails['city'] = $formData['city'];
            $userDetails->save();
            // Call payment service
            $paymentDetails = $this->getPaymentDetails($userDetails->id,$formData['iban'],$formData['account_owner']);
            // Save paymet details
            $payment = new UserPayment;
            $payment['user_id'] = $userDetails->id;
            $payment['account_owner'] = $formData['account_owner'];
            $payment['iban'] = $formData['iban'];
            // Here actual response need to be checked and do modification accordingly
            $payment['payment_data_id'] = isset($paymentDetails['paymentDataId'])?$paymentDetails['paymentDataId']: ''; 
            $payment->save();
            return json_encode($paymentDetails);
        } catch (Exception $exc) {
            \DB::rollBack();
        }
    }
    
    /**
     * Function to call the external api
     * @param type $customerId
     * @param type $iban
     * @param type $owner
     * @return type JSON
     */
    public function getPaymentDetails($customerId,$iban,$owner) {
        $response = Http::post('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-b
    ackend-dev-save-payment-data', [
                'customerId' => $customerId,
                'iban' => $iban,
                'owner' => $owner,
            ]);
        if($response->getStatusCode()== 200){
            return ['status'=>200,'message'=>$response->getBody()];
        } else if($response->getStatusCode()== 403) {
            // unauthorised
            return ['status'=>403,'message'=>json_decode($response->getBody()->getContents())->message];
        } else {
            return json_encode(['status'=>'4xx','message'=>'Something went wrong!']);
        }
    }
}
