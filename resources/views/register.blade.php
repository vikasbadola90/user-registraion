@extends('layouts/layout-user')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2 id="heading">Sign Up Your User Account</h2>
                <p>Fill all form field to go to next step</p>
                <form id="msform" method="POST">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" step="1" id="account"><strong>Personal</strong></li>
                        <li id="personal" step="2"><strong>Address</strong></li>
                        <li id="payment" step="3"><strong>Payment</strong></li>
                        <li id="confirm"><strong>Finish</strong></li>
                    </ul>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    <fieldset id="fieldset1">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Personal Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 1 - 4</h2>
                                </div>
                            </div> 
                            <label class="fieldlabels">First Name: *</label> 
                            <input type="text" name="first_name" placeholder="First Name" autocomplete="off" data-msg="Please enter your first name"  required=""/> 
                            <label class="fieldlabels">Last Name: *</label> 
                            <input type="text" name="last_name" placeholder="Last Name" data-msg="Please enter your last name" required="" /> 
                            <label class="fieldlabels">Telephone: *</label> 
                            <input type="text" name="telephone" placeholder="Telephone"  required=""/> 
                        </div> 
                        <button type="button" step="1" class="next action-button">Next</button>
                    </fieldset>
                    <fieldset id="fieldset2">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Address Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 2 - 4</h2>
                                </div>
                            </div> 
                            <label class="fieldlabels">Street: *</label> 
                            <input type="text" name="street" placeholder="Street" data-msg="Please enter your street" required=""/> 
                            <label class="fieldlabels">House Number: *</label> 
                            <input type="text" name="house_no" placeholder="House Number"  required=""/> 
                            <label class="fieldlabels">Zip Code: *</label> 
                            <input type="text" name="zip_code" placeholder="Zip Code" required=""/> 
                            <label class="fieldlabels">City: *</label> 
                            <input type="text" name="city" placeholder="City" data-msg="Please enter your city" required=""/>
                        </div> 
                        <button type="button" step="2" class="next action-button">Next</button>
                        <!--<input type="button" step="2" name="next" class="next action-button" value="Next" />--> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                    </fieldset>
                    <fieldset id="fieldset3">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Payment Information:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 3 - 4</h2>
                                </div>
                            </div> 
                            <label class="fieldlabels">Account Owner: *</label> 
                            <input type="text" name="account_owner" placeholder="Account Owner" data-msg="Please enter account owner" required=""/> 
                            <label class="fieldlabels">IBAN: *</label> 
                            <input type="text" name="iban" placeholder="IBAN" data-msg="Please enter your IBAN" required=""/> 
                        </div> 
                        <button step="3" class="next action-button" id="nextBtn" style="display:none">
                                Submit
                        </button>
                        <input type="submit" step="3" name="next" id="submitLast" class="action-button" value="Submit" /> 
                        <input type="button" name="previous" class="lastPrevious previous action-button-previous" value="Previous" />
                    </fieldset>
                    <fieldset id="fieldset4">
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Finish:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Step 4 - 4</h2>
                                </div>
                            </div> <br><br>
                            <h2 class="purple-text text-center"><strong>SUCCESS !</strong></h2> <br>
                            <div class="row justify-content-center">
                                <div class="col-3">
                                    Payment Id: <span id="paymentId"></span>
                                </div>
                            </div> <br><br>
                            <div class="row justify-content-center">
                                <div class="col-7 text-center">
                                    <h5 class="purple-text text-center">You Have Successfully Signed Up</h5>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection