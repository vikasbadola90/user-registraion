<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>User Registration</title>
        <!-- Custom fonts for this template-->
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/user-style.css')}}" rel="stylesheet">
    </head>
    <body>
        <!-- Main Content -->
        <div id="content">
            @yield('content')
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('js/jquery.min.js')}}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('js/register.js')}}"></script>
    </body>
</html>