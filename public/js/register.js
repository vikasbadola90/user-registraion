$(document).ready(function () {
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var steps = $("fieldset").length;

    if (localStorage.getItem("registrationStep") == 4) {
        localStorage.removeItem("registrationStep");
        localStorage.removeItem("registrationDetails");
    }

    if (localStorage.getItem("registrationStep")) {
        $("#progressbar li").slice(0, localStorage.getItem("registrationStep")).addClass("active");
        $("#fieldset" + localStorage.getItem("registrationStep")).show();
        $("#fieldset1").hide();
        var current = localStorage.getItem("registrationStep");
        setProgressBar(localStorage.getItem("registrationStep"));
        $.each(JSON.parse(localStorage.getItem("registrationDetails")), function (key, value) {
            $("input[name='" + value.name + "']").val(value.value);
        });
    } else {
        var current = 1;
        setProgressBar(current);
    }

    var v = $("#msform").validate({
        errorElement: "p",
        rules: {
            telephone: {
                required: true,
                digits: true
            },
            house_no: {
                required: true,
                digits: true
            },
            zip_code: {
                required: true,
                digits: true
            },
        },
        messages: {
            telephone: {required: "Please Enter Your Telephone Number",telephone:"Please enter numbers Only"},
            house_no: {required: "Please Enter Your House Number",house_no:"Please enter numbers Only"},
            zip_code: {required: "Please Enter Your Zip Code",zip_code:"Please enter numbers Only"},
        }
    });

    $(".next").click(function () {
        if (v.form()) {

            if (typeof (Storage) !== "undefined") {
                localStorage.setItem('registrationDetails', JSON.stringify($('#msform').serializeArray()));
                localStorage.setItem('registrationStep', parseInt($(this).attr('step')) + 1);
            }
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;
                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                },
                duration: 500
            });
            setProgressBar(++current);
        }
    });

    $(".previous").click(function () {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(--current);
    });

    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar").css("width", percent + "%");
    }

    $('#msform').on('submit', function (e) {
        if (v.form()) {
            e.preventDefault();
            var data = $(this).serializeArray(); // convert form to array
            $.ajax({
                url: "/register-user",
                type: "POST",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    data: $.param(data),
                },
                success: function (response) {
                    if(JSON.parse(response).status == 200){
                        $('#submitLast').hide();
                        $('#nextBtn').show();
                        $('#nextBtn').click();
                        $("#paymentId").html('ssss');
                    } else {
                        alert(JSON.parse(response).message);
                    }
                },
                error: function (reject) {
                    
                }

            });
        }
    });
});

