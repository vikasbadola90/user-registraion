<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


## Laravel Framework 9.8.1 - User Registration

## User Registration

This is user registration page with multi step form. The user data is stored into local storage just to make sure that user starts from the same step where they left the signup.

## Requirements

- Laravel 9.x requires a minimum PHP version of 8.0
- MYSQL DB
- If you are running into localhost, please create a virtual host so that it can work on https:// as we are calling an external api from here. To create a virtual host use below reference.

```php
<VirtualHost *:443>
    DocumentRoot C:/xampp/htdocs/user-registration-app/public
    ServerName user-registration-app.localhost
    SSLEngine on
    SSLCertificateFile "conf/ssl.crt/server.crt"
    SSLCertificateKeyFile "conf/ssl.key/server.key"
</VirtualHost>
```

## Setup

- Clone or download the code.
- cd to your downloaded code ```cd user-registraion```
- run ```composer install```
- rename .env.example to .env
- run ```php artisan key:generate```
- Setup your database into .env file
- Run migrations. Migrations can be found within database/migrations. 
```
php artisan migrate
```
- Setup virtual environment.
- Run the project.
```
php artisan serve --port=443
```
- This is it, you can now open ```https://user-registration-app.localhost``` to see the registration form.

## Possible performance optimizations

The code is very minimal so we can't do much about optimizations. But on the frontend side we could have use vue/react instead of bootstrap.

## Things could have been done better

I am storing user information into HTML5 local storage, this could have been saved somewhere else based on requiremet. Also there should be some backend validations. Right now it do not have any backend validations.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
